package com.idc.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="IDC_MST_STOK_BARANG")
public class IdcMstStokBarang implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_BARANG", length=13, nullable=false, unique=true)
	private String idBarang;
	
	@Column(name="DESC_BARANG",length=40)
	private String descBarang;
	
	@Column(name="HARGA_BARANG",precision=17, scale=2)
	private BigDecimal hargaBarang;
	
	public IdcMstStokBarang() {
		// TODO Auto-generated constructor stub
	}

	public String getIdBarang() {
		return idBarang;
	}

	public void setIdBarang(String idBarang) {
		this.idBarang = idBarang;
	}

	public String getDescBarang() {
		return descBarang;
	}

	public void setDescBarang(String descBarang) {
		this.descBarang = descBarang;
	}

	public BigDecimal getHargaBarang() {
		return hargaBarang;
	}

	public void setHargaBarang(BigDecimal hargaBarang) {
		this.hargaBarang = hargaBarang;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idBarang == null) ? 0 : idBarang.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdcMstStokBarang other = (IdcMstStokBarang) obj;
		if (idBarang == null) {
			if (other.idBarang != null)
				return false;
		} else if (!idBarang.equals(other.idBarang))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IdcMstStokBarang [idBarang=" + idBarang + "]";
	}
	

}
