package com.idc.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="IDC_HDR_SELLING")
public class IdcHdrSelling implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_SELLING", length=20, nullable=false, unique=true)
	private String idSelling;

	@Column(name="TGL_SELLING")
	@Temporal(TemporalType.DATE)
	private Date tglSelling;

	@Column(name="TOTAL_HARGA")
	private BigDecimal totalHarga;

	@OneToMany(cascade=CascadeType.ALL, mappedBy="idcHdrSelling")
	private Set<IdcDtlSelling> idcDtlSellingsSet;

	public IdcHdrSelling() {
		// TODO Auto-generated constructor stub
	}

	public String getIdSelling() {
		return idSelling;
	}

	public void setIdSelling(String idSelling) {
		this.idSelling = idSelling;
	}

	public Date getTglSelling() {
		return tglSelling;
	}

	public BigDecimal getTotalHarga() {
		return totalHarga;
	}

	public void setTotalHarga(BigDecimal totalHarga) {
		this.totalHarga = totalHarga;
	}

	public void setTglSelling(Date tglSelling) {
		this.tglSelling = tglSelling;
	}

	public Set<IdcDtlSelling> getIdcDtlSellingsSet() {
		return idcDtlSellingsSet;
	}

	public void setIdcDtlSellingsSet(Set<IdcDtlSelling> idcDtlSellingsSet) {
		this.idcDtlSellingsSet = idcDtlSellingsSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idSelling == null) ? 0 : idSelling.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdcHdrSelling other = (IdcHdrSelling) obj;
		if (idSelling == null) {
			if (other.idSelling != null)
				return false;
		} else if (!idSelling.equals(other.idSelling))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IdcHdrSelling [idSelling=" + idSelling + "]";
	}


}
