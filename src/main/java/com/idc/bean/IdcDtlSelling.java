package com.idc.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="IDC_DTL_SELLING")
public class IdcDtlSelling implements Serializable{
	

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_SELLING", length=20, nullable=false)
	private String idSelling;
	
	@Id
	@Column(name="ID_BARANG", length=13, nullable=false)
	private String idBarang;
	
	@Column(name="DESC_BARANG",length=40)
	private String descBarang;
	
	@Column(name="HARGA_BARANG",precision=17, scale=2)
	private BigDecimal hargaBarang;
	
	@Column(name="QTY_JUAL")
	private int qtyJual;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SELLING", referencedColumnName="ID_SELLING", insertable=false, updatable=false)
	private IdcHdrSelling idcHdrSelling;
	
	public IdcDtlSelling() {
		// TODO Auto-generated constructor stub
	}

	public String getIdSelling() {
		return idSelling;
	}

	public void setIdSelling(String idSelling) {
		this.idSelling = idSelling;
	}

	public String getIdBarang() {
		return idBarang;
	}

	public void setIdBarang(String idBarang) {
		this.idBarang = idBarang;
	}

	public String getDescBarang() {
		return descBarang;
	}

	public void setDescBarang(String descBarang) {
		this.descBarang = descBarang;
	}

	public BigDecimal getHargaBarang() {
		return hargaBarang;
	}

	public void setHargaBarang(BigDecimal hargaBarang) {
		this.hargaBarang = hargaBarang;
	}

	public int getQtyJual() {
		return qtyJual;
	}

	public void setQtyJual(int qtyJual) {
		this.qtyJual = qtyJual;
	}

	public IdcHdrSelling getIdcHdrSelling() {
		return idcHdrSelling;
	}

	public void setIdcHdrSelling(IdcHdrSelling idcHdrSelling) {
		this.idcHdrSelling = idcHdrSelling;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idBarang == null) ? 0 : idBarang.hashCode());
		result = prime * result
				+ ((idSelling == null) ? 0 : idSelling.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdcDtlSelling other = (IdcDtlSelling) obj;
		if (idBarang == null) {
			if (other.idBarang != null)
				return false;
		} else if (!idBarang.equals(other.idBarang))
			return false;
		if (idSelling == null) {
			if (other.idSelling != null)
				return false;
		} else if (!idSelling.equals(other.idSelling))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IdcDtlSelling [idSelling=" + idSelling + "]";
	}
	
	
}
