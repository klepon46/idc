package com.idc.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.idc.bean.IdcHdrSelling;

@Repository
public class IdcHdrSellingDaoImpl implements IdcHdrSellingDao {

	@Autowired
	SessionFactory sessionFactory;

	public void save(IdcHdrSelling domain, String user) {
		sessionFactory.getCurrentSession().save(domain);
	}

	public void delete(IdcHdrSelling domain) {
		// TODO Auto-generated method stub

	}

	public void update(IdcHdrSelling domain, String user) {
		// TODO Auto-generated method stub

	}

	public List<IdcHdrSelling> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
