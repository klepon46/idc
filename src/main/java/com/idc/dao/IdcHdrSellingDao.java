package com.idc.dao;

import com.idc.bean.IdcHdrSelling;
import com.idc.util.IdcGenericDao;

public interface IdcHdrSellingDao extends IdcGenericDao<IdcHdrSelling> {

}
