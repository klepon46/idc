package com.idc.dao;

import com.idc.bean.IdcMstStokBarang;
import com.idc.util.IdcGenericDao;

public interface IdcMstStokBarangDao extends IdcGenericDao<IdcMstStokBarang> {

}
