package com.idc.dao;

import com.idc.bean.IdcDtlSelling;
import com.idc.util.IdcGenericDao;

public interface IdcDtlSellingDao extends IdcGenericDao<IdcDtlSelling> {

}
