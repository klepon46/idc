package com.idc.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.idc.bean.IdcMstStokBarang;

@Repository
public class IdcMstStokBarangDaoImpl implements IdcMstStokBarangDao {

	@Autowired
	SessionFactory sessionFactory;
	
	public void save(IdcMstStokBarang domain, String user) {
		sessionFactory.getCurrentSession().save(domain);
	}

	public void delete(IdcMstStokBarang domain) {
		// TODO Auto-generated method stub
		
	}

	public void update(IdcMstStokBarang domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	public List<IdcMstStokBarang> findAll() {
		return sessionFactory.getCurrentSession().createQuery("from IdcMstStokBarang").list();
	}

	
	
}
