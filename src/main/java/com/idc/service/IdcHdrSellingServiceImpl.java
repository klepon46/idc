package com.idc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.idc.bean.IdcHdrSelling;
import com.idc.dao.IdcHdrSellingDao;

@Service
@Transactional
public class IdcHdrSellingServiceImpl implements IdcHdrSellingService{

	@Autowired
	IdcHdrSellingDao dao;
	
	public void save(IdcHdrSelling domain, String user) {
		dao.save(domain, user);
	}

	public void delete(IdcHdrSelling domain) {
		// TODO Auto-generated method stub
		
	}

	public void update(IdcHdrSelling domain, String user) {
		// TODO Auto-generated method stub
		
	}

	public List<IdcHdrSelling> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
