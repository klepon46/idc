package com.idc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.idc.bean.IdcMstStokBarang;
import com.idc.dao.IdcMstStokBarangDao;

@Service
@Transactional
public class IdcMstStokBarangServiceImpl implements IdcMstStokBarangService{

	@Autowired
	IdcMstStokBarangDao dao;
	
	public void save(IdcMstStokBarang domain, String user) {
		dao.save(domain, user);
	}

	public void delete(IdcMstStokBarang domain) {
		// TODO Auto-generated method stub
		
	}

	public void update(IdcMstStokBarang domain, String user) {
		// TODO Auto-generated method stub
		
	}

	public List<IdcMstStokBarang> findAll() {
		return dao.findAll();
	}

}
