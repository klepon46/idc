package com.idc.service;

import com.idc.bean.IdcDtlSelling;
import com.idc.util.IdcGenericService;

public interface IdcDtlSellingService extends IdcGenericService<IdcDtlSelling> {

}
