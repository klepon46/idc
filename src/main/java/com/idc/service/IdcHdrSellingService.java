package com.idc.service;

import com.idc.bean.IdcHdrSelling;
import com.idc.util.IdcGenericService;

public interface IdcHdrSellingService extends IdcGenericService<IdcHdrSelling> {

}
