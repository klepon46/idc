package com.idc.helper;

import java.math.BigDecimal;

public class IdcSellingHelper {

	private String kodeBarang;
	private String descBarang;
	private BigDecimal hargaBarang;
	private int qty;
	
	public IdcSellingHelper() {
		// TODO Auto-generated constructor stub
	}
	
	public String getKodeBarang() {
		return kodeBarang;
	}
	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}
	public String getDescBarang() {
		return descBarang;
	}
	public void setDescBarang(String descBarang) {
		this.descBarang = descBarang;
	}
	public BigDecimal getHargaBarang() {
		return hargaBarang;
	}
	public void setHargaBarang(BigDecimal hargaBarang) {
		this.hargaBarang = hargaBarang;
	}
	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}
	
	
	
}
