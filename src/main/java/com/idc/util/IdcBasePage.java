package com.idc.util;

import org.zkoss.zkplus.spring.SpringUtil;

public class IdcBasePage {

	private IdcMstService idcMstService;

	public IdcMstService getIdcMstService() {
		this.idcMstService = (IdcMstService) SpringUtil.getBean("idcMstService");
		return idcMstService;
	}

	public void setIdcMstService(IdcMstService idcMstService) {
		this.idcMstService = idcMstService;
	}
	
	
	
}
