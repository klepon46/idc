package com.idc.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.idc.service.IdcDtlSellingService;
import com.idc.service.IdcHdrSellingService;
import com.idc.service.IdcMstStokBarangService;

@Component
public class IdcMstService {

	@Autowired
	private IdcDtlSellingService idcDtlSellingService;
	
	@Autowired
	private IdcHdrSellingService idcHdrSellingService;
	
	@Autowired
	private IdcMstStokBarangService idcMstStokBarangService;

	public IdcDtlSellingService getIdcDtlSellingService() {
		return idcDtlSellingService;
	}

	public void setIdcDtlSellingService(IdcDtlSellingService idcDtlSellingService) {
		this.idcDtlSellingService = idcDtlSellingService;
	}

	public IdcHdrSellingService getIdcHdrSellingService() {
		return idcHdrSellingService;
	}

	public void setIdcHdrSellingService(IdcHdrSellingService idcHdrSellingService) {
		this.idcHdrSellingService = idcHdrSellingService;
	}

	public IdcMstStokBarangService getIdcMstStokBarangService() {
		return idcMstStokBarangService;
	}

	public void setIdcMstStokBarangService(
			IdcMstStokBarangService idcMstStokBarangService) {
		this.idcMstStokBarangService = idcMstStokBarangService;
	}
	
}
