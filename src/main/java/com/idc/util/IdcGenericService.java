package com.idc.util;

import java.util.List;

public interface IdcGenericService<T> {

	public void save(T domain, String user);
	public void delete(T domain);
	public void update (T domain, String user);
	public List<T> findAll();
	
}
