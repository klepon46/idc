package com.idc.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class IdcIdGenerator {

	public static String generateId(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmssS");
		return sdf.format(new Date());
	}
	
}
