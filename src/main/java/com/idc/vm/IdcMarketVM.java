package com.idc.vm;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelList;

import com.idc.bean.IdcDtlSelling;
import com.idc.bean.IdcHdrSelling;
import com.idc.bean.IdcMstStokBarang;
import com.idc.helper.IdcSellingHelper;
import com.idc.util.IdcBasePage;
import com.idc.util.IdcIdGenerator;

public class IdcMarketVM extends IdcBasePage {

	private int qtyJual;
	private BigDecimal total;
	private IdcMstStokBarang barang;
	private IdcMstStokBarang barangBaru;

	private ListModelList<IdcMstStokBarang> listModelBarang;
	private ListModelList<IdcSellingHelper> listModelHelper;

	@Init
	public void init(){
		total = new BigDecimal(0);
	}

	@Command
	@NotifyChange({"listModelHelper","total"})
	public void add(){
		if(qtyJual == 0){
			Clients.showNotification("Qty tidak boleh 0", Clients.NOTIFICATION_TYPE_ERROR, null, "top_right", 2000);
			return;
		}

		IdcSellingHelper helper = new IdcSellingHelper();
		helper.setKodeBarang(getBarang().getIdBarang());
		helper.setDescBarang(getBarang().getDescBarang());
		helper.setHargaBarang(getBarang().getHargaBarang());
		helper.setQty(getQtyJual());

		listModelHelper.add(helper);
		
		total = total.add(helper.getHargaBarang());
	}


	@Command
	@NotifyChange({"barang","barangBaru","listModelBarang"})
	public void saveBarang(){
		if(barangBaru.getIdBarang() == null){
			Clients.showNotification("Id tidak boleh null", Clients.NOTIFICATION_TYPE_ERROR, null, "top_right", 2000);
			return;
		}
		
		if(barangBaru.getDescBarang() == null){
			Clients.showNotification("Desc barang tidak boleh null", Clients.NOTIFICATION_TYPE_ERROR, null, "top_right", 2000);
			return;
		}
		
		if(barangBaru.getHargaBarang() == null){
			Clients.showNotification("Harga barang tidak boleh null", Clients.NOTIFICATION_TYPE_ERROR, null, "top_right", 2000);
			return;
		}
		
		getIdcMstService().getIdcMstStokBarangService().save(barangBaru, "ANANG");
		barangBaru = new IdcMstStokBarang();
		Clients.showNotification("Berhasil Tambah Barang", Clients.NOTIFICATION_TYPE_INFO, null, "top_right", 2000);
		
		//update list barang
		listModelBarang = new ListModelList<IdcMstStokBarang>();
		List<IdcMstStokBarang> list = getIdcMstService().getIdcMstStokBarangService().findAll();
		listModelBarang.addAll(list);
	}

	@Command
	public void simpan(){
		String id = IdcIdGenerator.generateId();

		IdcHdrSelling hdrSelling = new IdcHdrSelling();
		hdrSelling.setIdSelling(id);

		Set<IdcDtlSelling> dtlSellingSet = new HashSet<IdcDtlSelling>();

		for(IdcSellingHelper current : listModelHelper){

			IdcDtlSelling dtlSelling = new IdcDtlSelling();

			dtlSelling.setIdSelling(hdrSelling.getIdSelling());
			dtlSelling.setIdBarang(current.getKodeBarang());
			dtlSelling.setDescBarang(current.getDescBarang());
			dtlSelling.setHargaBarang(current.getHargaBarang());
			dtlSelling.setQtyJual(current.getQty());

			dtlSellingSet.add(dtlSelling);
		}

		hdrSelling.setTglSelling(new Date());
		hdrSelling.setTotalHarga(getTotal());
		hdrSelling.setIdcDtlSellingsSet(dtlSellingSet);
		getIdcMstService().getIdcHdrSellingService().save(hdrSelling, "Anang");

		Clients.showNotification("Berhasil Simpan dengan nomor transaksi = " + hdrSelling.getIdSelling(), 
				Clients.NOTIFICATION_TYPE_INFO, null, "top_right", 2000);

	}

	public ListModelList<IdcMstStokBarang> getListModelBarang() {
		if(listModelBarang == null){
			listModelBarang = new ListModelList<IdcMstStokBarang>();
			List<IdcMstStokBarang> list = getIdcMstService().getIdcMstStokBarangService().findAll();
			listModelBarang.addAll(list);
		}

		return listModelBarang;
	}

	public void setListModelBarang(ListModelList<IdcMstStokBarang> listModelBarang) {
		this.listModelBarang = listModelBarang;
	}

	public IdcMstStokBarang getBarang() {
		return barang;
	}

	public void setBarang(IdcMstStokBarang barang) {
		this.barang = barang;
	}

	public ListModelList<IdcSellingHelper> getListModelHelper() {
		if(listModelHelper == null){
			listModelHelper = new ListModelList<IdcSellingHelper>();
		}
		return listModelHelper;
	}

	public void setListModelHelper(ListModelList<IdcSellingHelper> listModelHelper) {
		this.listModelHelper = listModelHelper;
	}

	public int getQtyJual() {
		return qtyJual;
	}

	public void setQtyJual(int qtyJual) {
		this.qtyJual = qtyJual;
	}

	public IdcMstStokBarang getBarangBaru() {
		if(barangBaru == null){
			barangBaru = new IdcMstStokBarang();
		}
		return barangBaru;
	}

	public void setBarangBaru(IdcMstStokBarang barangBaru) {
		this.barangBaru = barangBaru;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
